    'use strict'; //model definitions
    var ContactInformation = {        
        firstName: String,
        lastName: String,
        dob: Date,
        phone: String,
        email: String,
        address: {
            line1: String,
            line2: String,
            city: String,
            state: String,
            zip: String
        }
    };
    module.exports = {
        Client: mongoose.model("Client", mongoose.Schema(ContactInformation));
    };