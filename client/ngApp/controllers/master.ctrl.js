angular.module('CecDareToCareApp').controller('masterCtrl', ['$scope', '$rootScope', '$location', '$routeParams', '$route', function($scope, $rootScope, $location, $routeParams, $route) {
  
    $scope.routeParams = $routeParams;
    $scope.AuthenticatedUser = {
        userName: 'jh1',
        firstName: 'Johnny',
        lastName: 'Happy',
        accessibleModules: [{
            name: "Today's Queue",
            url: '/queue'
        }, {
            name: 'Client Management',
            url: '/client'
        }, 
        {
            name: 'Admin Options',
            url: '/admin'
        },
        {
            name: 'Login',
            url: '/login'
        }]

    };

    $scope.AppData = {
        appName: 'C.E.C. Dare To Care Client Manager',
        currentModule: null
    };

    $scope.updateCurrentModule = function(currentRoute) {
        for (var i = $scope.AuthenticatedUser.accessibleModules.length; i--;) {
            if ($scope.AuthenticatedUser.accessibleModules[i].url === currentRoute) {
                $scope.AppData.currentModule = $scope.AuthenticatedUser.accessibleModules[i];
                break;
            }
        }
    };

    $scope.login = function() {
        for (var i = $scope.AuthenticatedUser.accessibleModules.length; i--;) {
            if ($scope.AuthenticatedUser.accessibleModules[i].name === 'Login') {
                $scope.AuthenticatedUser.currentModule = $scope.AuthenticatedUser.accessibleModules[i];
                break;
            }
        }
    };

    //when the routing has succeeded
    $rootScope.$on("$routeChangeSuccess", function(event, current, previous, rejection) {
        $scope.updateCurrentModule($location.path());
    });

}]);