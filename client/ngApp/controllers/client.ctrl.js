angular.module('CecDareToCareApp').controller('clientCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {

	$scope.FindData = {
		first: null,
		last: null,
		bday: null,
		visit: null
	};

	$scope.newServiceDate = null;

	$scope.Clients = [];

	$scope.ClintIdx = 0;

	$scope.selectCustomer = function(idx) {
		$scope.ClintIdx = idx;
	};

	$scope.resetSearchForm = function() {
		$scope.FindData = {
			first: null,
			last: null,
			bday: null,
			visit: null
		};
	};

	$scope.sendFindData = function() {
		$scope.Clients = [];
		var f = $scope.FindData.first || '_none',
			l = $scope.FindData.last || '_none',
			b = moment($scope.FindData.bday, 'MM/DD/YYYY');
		if (b.isValid()) {
			b = b.format('YYYY-MM-DD');
		}
		else {
			b = '_none';
		}

		var url = '/api/findcustomer/' + f + '/' + l + '/' + b;
		$http.get(url)
			.success(function(dataRows, status, headers, config) {
				dataRows.forEach(function(row) {
					$scope.Clients.push({
						id: row.id,
						first: row.first_name,
						last: row.last_name,
						bday: (row.dob) ? moment(row.dob).format('YYYY-MM-DD') : null,
						visits: [],
						notes: null,
						address: {
							line1: row.addr_line1,
							line2: row.addr_line2,
							city: row.city,
							state: row.state,
							zip: row.zip
						}
					});
					//now get the service dates
					$scope.getServiceDates(row.id);
				});
			});
	}

	$scope.resetData = function() {
		$scope.Clients = [];
	};

	$scope.sendClientEdit = function() {
		alert('sendClientEdit');
	};

	$scope.getQueue = function() {
		$http.get('/api/clients').success(function(data, status, headers, config) {

		}).error(function(data, status, headers, config) {
			console.error({
				Data: data,
				Status: status,
				Headers: headers,
				Config: config
			});
		})
	};

	$scope.getServiceDates = function(customer_id) {
		var url = '/api/customerservicedates/' + customer_id;
		$http.get(url)
			.success(function(dataRows, status, headers, config) {
				dataRows.forEach(function(row) {
					if ($scope.Clients[$scope.ClintIdx]) {
						$scope.Clients[$scope.ClintIdx].visits = [];
						$scope.Clients[$scope.ClintIdx].visits.push(row);
					}
				});
			})
			.error(function(data, status, headers, config) {
				console.error({
					Data: data,
					Status: status,
					Headers: headers,
					Config: config
				});
			});
	};

	var x = [];
	$scope.addServiceDate = function(customer_id) {
		$('#popup').dialog({
			autoOpen: true,
			width: 350,
			modal: true,
			beforeClose: function(evt, ui) {
				var d = moment($scope.newServiceDate, 'MM/DD/YYYY');
				$scope.Clients[$scope.ClintIdx].visits.push({
					"id": null,
					"customer_id": $scope.Clients[$scope.ClintIdx].id,
					"checkin_tm": null,
					"checkout_tm": null,
					"canceled": 0,
					"pickup_items": null,
					"dt": d.format('YYYY-MM-DD'),
					"tm": d.format('HH:mm:ss'),
					"creator": 'sys',
					"createDt": moment().format()
				});
				//prompt ui to redraw
				$scope.$apply();
			}
		});
	};
}]);