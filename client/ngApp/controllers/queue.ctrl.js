angular.module('CecDareToCareApp').controller('queueCtrl', ['$scope', '$http', function($scope, $http) {
	$scope.Data = [];

	$scope.getData = function() {
		$http.get('/api/queue/' + moment().startOf('hour').format('YYYY-MM-DD')).success(function(dataRows, status, headers, config) {

			angular.forEach(dataRows, function(data) {
				$scope.Data.push({
					served: data.checkout_tm !== null,
					canceled: data.canceled,
					first: data.first_name,
					last: data.last_name,
					pickup: (data.pickup_items !== null) ? moment(data.pickup_items).format('YYYY-MM-DD') : '',
					timeIn: (data.checkin_tm !== null) ? moment(data.checkin_tm, 'HH:mm:ss').format('HH:mm a') : ''
				});
			});
		}).error(function(data, status, headers, config) {
			console.error('getData failed with status ' + status + ': ' + data);
		});
	};

	$scope.getData();

}]);