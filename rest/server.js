var appName = 'cec dare to care manager',
  myPort = process.env.PORT || 3000,
  myIp = process.env.IP || '0.0.0.0',
  path = require("path"),
  url = require("url"),
  moment = require('moment'),
  express = require('express'),
  app = express(),
  db = require("./mods/db");


//handle non web api request
app.use(express.static(path.resolve(__dirname, '../client')));

//the REST api
app.get('/api/clients', function(req, res) {
  'use strict';
  console.info(req.method + ':' + url.parse(req.url).pathname);

  var qry = {};

  db.getClients({}, function(err, row) {
    if (!err)
      res.json(row);
  });
});

app.get('/api/queue/:day', function(req, res) {
  'use strict';
  var methodSig = req.method + ':' + url.parse(req.url).pathname;
  console.info(methodSig);
  var day = '2015-01-03'; //req.params.day || moment().startOf('hour').format('YYYY-MM-DD');   
  var rows = [];

  db.GetQueue(day, function(err, row) {
    rows.push(row);
  }, function(err, rowCount) {
    console.info(methodSig + ' retrieved ' + rowCount + ' rows');
    res.json(rows);
  });

});

app.get('/api/findcustomer/:first/:last/:dob', function(req, res) {
  'use strict';
  var methodSig = req.method + ':' + url.parse(req.url).pathname + ' params = ' + JSON.stringify(req.params);
  console.info(methodSig);

  var first = (req.params.first || ''),
    last = (req.params.last || ''),
    dob = (req.params.dob || '');

  first = first.replace('_none', '') + '%',
    last = last.replace('_none', '') + '%',
    dob = (dob ==='_none')? null : dob;

  console.info('sql params = ' + JSON.stringify({
    $first: first,
    $last: last,
    $dob: dob
  }));

  var rows = [];

  db.FindCustomer(first, last, dob, function(err, row) {
    rows.push(row);
  }, function(err, rowCount) {
    console.info(methodSig + ' retrieved ' + rowCount + ' rows with err = ' + err);
    res.json(rows);
  });
});

app.get('/api/customer/:id', function(req, res) {
  'use strict';
  var methodSig = req.method + ':' + url.parse(req.url).pathname + ' params = ' + JSON.stringify(req.params);
  console.info(methodSig);

  var cust = null;
  db.GetCustomer(req.params.id, function(err, row) {
    cust = row;
  }, function(err, rowCount) {
    console.info(methodSig + ' retrieved ' + rowCount + ' rows');
    res.json(cust);
  });


});

app.get('/api/customerservicedates/:customerId', function(req, res) {
  'use strict';
  var methodSig = req.method + ':' + url.parse(req.url).pathname + ' params = ' + JSON.stringify(req.params);
  console.info(methodSig);

  var rows = [];
  db.GetCustomerServiceDates(parseInt(req.params.customerId, 10), function(err, row) {
    if (err) {
      console.info(methodSig + ' error: ' + err);
    }
    else
      rows.push(row);
  }, function(err, rowCount) {
    console.info(methodSig + 'retrieved ' + rowCount + ' rows');
    res.json(rows);
  });

})

app.post('/api/customerservicedate/:customerId/:newdate',function(req, res){
  
});



app.listen(myPort, myIp, function() {
  console.log(appName + " listening running on " + myIp + ":" + myPort);
});
