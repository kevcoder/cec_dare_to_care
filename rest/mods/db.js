var sql = require('sqlite3'),
    fs = require("fs"),
    db = new sql.Database('cec.db'),
    createStmts = [{
        'name': 'customer',
        'sql': '\
create table customer( \
id INTEGER PRIMARY KEY, \
first_name VARCHAR(50), \
last_name VARCHAR(50), \
dob DATE, \
addr_line1 VARCHAR(50), \
addr_line2 VARCHAR(50), \
city VARCHAR(50), \
state VARCHAR(2), \
zip VARCHAR(10) \
);'
    }, {
        'name': 'service_date',
        'sql': 'create table service_date( \
id INTEGER PRIMARY KEY, \
customer_id INTEGER REFERENCES customer(id), \
checkin_tm TIME CURRENT_TIME, \
checkout_tm TIME, \
canceled BIT DEFAULT 0, \
pickup_items VARCHAR(100), \
dt DATE DEFAULT CURRENT_DATE, \
tm TIME DEFAULT CURRENT_TIME, \
creator VARCHAR(50), \
createDt DATETIME DEFAULT CURRENT_TIMESTAMP \
);'
    }],
    checkTableExistance = "select count(*) as tblexists from sqlite_master where type='table' and name=?;",
    selectStatements = [{
        'name': 'customer_search',
        'sql': 'select c.* from customer c where first_name like  $first_name and last_name like $last_name and dob = COALESCE($dob, dob) ORDER BY last_name, first_name'
    }, {
        'name': 'customer_detail',
        'sql': 'select c.* , s.dt service_date \
        from customer c join service_date s on c.id = s.customer_id \
        where c.id = $customer_id \
        order by s.dt DESC'
    }, {
        'name': 'customers_by_service_date',
        'sql': 'SELECT c.*, d.dt service_date, d.tm service_time FROM customer c JOIN service_date d ON c.id = d.customer_id WHERE d.dt >= $dt'
    }, {
        'name': 'service_date_by_customer',
        'sql': ' SELECT * FROM service_date s where s.customer_id = $customer_id '
    }, {
        'name': 'queue_by_day',
        'sql': 'SELECT c.first_name, c.last_name, s.id, s.checkin_tm, s.checkout_tm, s.canceled, s.pickup_items FROM service_date s JOIN customer c on s.customer_id = c.id WHERE s.dt = $dt ORDER by s.checkin_tm ASC'
    }, {
        'name': 'add_customer',
        'sql': 'INSERT INTO customer(first_name, last_name, dob, addr_line1, addr_line2, city, state, zip) \
        VALUES ($first_name, $last_name, $dob, $addr_line1, $addr_line2, $city, $state, $zip)'
    }, {
        'name': 'update_customer',
        'sql': 'UPDATE customer set first_name = $first_name, last_name = $last_name, dob =$dob, addr_line1 =  $addr_line1, addr_line2 = $addr_line2, city =$city, state = $state, zip = $zip WHERE id = $id'
    }, {
        'name': 'check_customer',
        'sql': 'SELECT count(1) FROM customer c where c.id = $id'
    }, {
        'name': 'add _service_date',
        'sql': 'INSERT INTO service_date(customer_id , checkin_tm , checkout_tm , canceled , pickup_items , dt , tm , creator , createDt  ) ) \
        VALUES ($customer_id , $checkin_tm , $checkout_tm , $canceled , $pickup_items , $dt , $tm , $creator , $createDt )'
    }, {
        'name': 'update_service_date',
        'sql': 'UPDATE service_date SET customer_id = $customer_id , checkin_tm = $checkin_tm , checkout_tm = $checkout_tm , canceled = $canceled , pickup_items= $pickup_items , dt = $dt , tm = $tm , creator = $creator , createDt = $createDt WHERE id = $id'
    }, {
        'name': 'check_service_date',
        'sql': 'SELECT count(1) FROM service_date s WHERE s.id = $id'
    }];


createStmts.forEach(function(s) {
    //initialize the database and/or tables if they do not exists
    db.serialize(function() {
        console.info('check for table ' + s.name);
        db.get(checkTableExistance, {
            "1": s.name
        }, function(err, row) {
            if (err) {
                console.error(err);
            }
            else {
                if (row.tblexists === 0) {
                    console.info('creating table ' + s.name);
                    db.run(s.sql);
                }
                else {
                    console.info(s.name + ' already exists');
                }
            }
        });
    });
});

function getRows(sql, namedParameters, clbk, clbkDone) {
    var sqlType = typeof(sql),
        paramType = typeof(namedParameters),
        clbkType = typeof(clbk),
        clbkDoneType = typeof(clbkDone);
    if (sqlType === 'undefined' || sqlType === 'null' || paramType === 'undefine') {
        if (clbkDoneType === 'function') {
            clbkDone('parameters sql and namedParameters are required', 0);
        }
        return;
    }

    db.each(sql, namedParameters, function(err, row) {
        if (clbkType === 'function')
            clbk(err, row);
    }, function(err, rowCount) {
        if (clbkDoneType === 'function') {
            clbkDone(err, rowCount);
        }
    });
};

function runSql(sql, namedParameters, clbk) {
    var sqlType = typeof(sql),
        paramType = typeof(namedParameters),
        clbkType = typeof(clbk);
    if (sqlType === 'undefined' || sql === null || paramType === 'undefine') {
        if (clbkType === 'function')
            clbk('parameters sql and namedParameters are required');
        return;
    }
    db.run(sql, namedParameters, function(err) {
        if (clbkType === 'function')
            clbk(err, this.lastId);
    });
};

function getPreparedSql(name) {
    var sql = '';
    selectStatements.some(function(e) {

        if ((e.name || '').trim().toLowerCase() === name.trim().toLowerCase()) {
            sql = e.sql;
            return true;
        }
    });
    return sql;
};

function getQueue(day, clbk, doneClbk) {
    getRows(getPreparedSql('queue_by_day'), {
        $dt: day
    }, clbk, doneClbk);
};

function findCustomers(first_name, last_name, dob, clbk, doneClbk) {
    getRows(getPreparedSql('customer_search'), {
        $first_name: first_name,
        $last_name: last_name,
        $dob: dob
    }, clbk, doneClbk);

}

function getCustomer(id, clbk, doneClbk) {
    getRows(getPreparedSql('customer_detail'), {
        $customer_id: id
    }, clbk, doneClbk);
}

function getCustomerServiceDates(customer_id, clbk, doneClbk) {
    getRows(getPreparedSql('service_date_by_customer'), {
        $customer_id: customer_id
    }, clbk, doneClbk);

};

function saveCustomer(customer, clbk, doneClbk) {
    db.serialize(function() {
        var doCustUpdate = false,
            dbErr = null,
            newPk = 0,
            clbkIsFunc = typeof(clbk) === 'function',
            doneClbkIsFunc = typeof(doneClbk) === 'function';

        // 1. are we updating or adding a record
        db.all(getPreparedSql('check_customer'), {
            $id: customer.id
        }, function(err, rows) {
            if (err) {
                dbErr = err;
                doneClbk(err, null);
                return;
            }
            else {
                doCustUpdate = rows[0] > 0;
            }
        });

        //2. build the parameter lists
        var params = {
            $id: customer.id,
            $first_name: customer.first_name,
            $last_name: customer.last_name,
            $dob: customer.dob,
            $addr_line1: customer.addr_line1,
            $addr_line2: customer.addr_line2,
            $city: customer.city,
            $state: customer.state,
            $zip: customer.zip
        };

        if (doCustUpdate) {
            //run an update statement
            runSql(getPreparedSql('update_customer'), params, function(err) {
                if (err && clbkIsFunc) {
                    dbErr = err;
                    clbk(err);
                    return;
                }
            });
        }
        else {
            //run an insert statement
            runSql(getPreparedSql('add_customer'), params, function(err, lastId) {
                if (err && doneClbkIsFunc) {
                    dbErr = err;
                    doneClbk(err, null);
                    return;
                }
                else {
                    newPk = lastId;
                }
            });
        }

        //3. if we've got no errors
        if (dbErr === null) {
            //return the new/updated row to the user
            getCustomer(newPk, clbk, doneClbk);
        }
        else {
            //report the error
            console.info('saveCustomer err: ' + JSON.stringify(dbErr));
        }
    });
};

function saveCustomerServiceDate()



module.exports = {
    Name: "sqlite3",
    GetQueue: getQueue,
    FindCustomer: findCustomers,
    GetCustomer: getCustomer,
    GetCustomerServiceDates: getCustomerServiceDates

};