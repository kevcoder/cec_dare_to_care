 angular.module('CecDareToCareApp', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
			.when('/queue', {
				'controller': 'queueCtrl',
				'templateUrl': './ngApp/views/queue.vws.htm'
			})
			.when('/queue/add', {
				'controller': 'queueCtrl',
				'templateUrl': './ngApp/views/queueadd.vws.htm'
			})
			.when('/client', {
				'controller': 'clientCtrl',
				'templateUrl': './ngApp/views/client.vws.htm'
			})
			.when('/login', {
				'controller': 'loginCtrl',
				'templateUrl': './ngApp/views/login.vws.htm'
			})
			.otherwise({
				'templateUrl': './ngApp/views/404.htm'
			});
	}]);